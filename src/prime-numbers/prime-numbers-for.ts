export function primeNumberFor(n: number): number {
  let count = [];

  for (let i = 2; i <= n; i++) {
    if (isPrime(i)) {
      count.push(i);
    }
  }

  console.log(count);
  return count.length;
}


function isPrime(val: number): boolean {
  let q = 0;

  for (let i = 1; i <= val; i++) {
    if (val % i === 0) {
      q++
    }
  }

  return q === 2;
}
