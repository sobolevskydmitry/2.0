export function primeNumbersImproved(n: number): number {
  let count = [];

  for (let i = 2; i <= n; i++) {
    if (isPrime(i)) {
      count.push(i);
    }
  }

  console.log(count)
  return count.length;
}


function isPrime(val: number): boolean {
  const sqrt = Math.sqrt(val);

  if (val === 2) return true;
  if (val % 2 === 0) return false;

  for (let i = 3; i <= sqrt; i += 2) {
    if (val % i === 0) {
      return false;
    }
  }

  return true;
}
