// @ts-ignore
import {performance} from 'perf_hooks';


export function performanceRec(func: Function): void {
  const t0 = performance.now();
  
  console.log(func());

  const t1 = performance.now();

  console.log(
    "\n---------------------------------------\n" +
    "Call took " + (t1 - t0) + " milliseconds." +
    "\n---------------------------------------\n\n"
  )
}
