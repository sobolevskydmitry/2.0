import {performanceRec} from './common/performanceCounter';
import {expBySquaring} from './src/exponentiation/exponentiation-by-squaring';
import {exponentiation} from './src/exponentiation/exponentiation';
import * as bigInt from 'big-integer';
import {fibonachi} from './src/fibonachi/fibonachi';
import {fibonachiFor} from './src/fibonachi/fibonachi-for';
import {fibonacciGoldenRation} from './src/fibonachi/fibonachi-golden-ration';
import {primeNumberFor} from './src/prime-numbers/prime-numbers-for';
import {primeNumbersImproved} from './src/prime-numbers/prime-numbers-for-improved';
import {sieveOfEratosthenes} from './src/prime-numbers/sieve-of-eratosthenes';


performanceRec(exponentiation.bind(this, 2, 1000000));
performanceRec(expBySquaring.bind(this, bigInt(2), 1000000));

performanceRec(fibonachi.bind(this,  1000000))
performanceRec(fibonachiFor.bind(this,  1000000))
performanceRec(fibonacciGoldenRation.bind(this,  1000000))


performanceRec(primeNumberFor.bind(this, 100000))
performanceRec(primeNumbersImproved.bind(this,  1000000))
performanceRec(sieveOfEratosthenes.bind(this,  1000000))
