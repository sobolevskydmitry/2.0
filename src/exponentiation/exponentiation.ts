import * as bigInt from 'big-integer';
import {BigNumber} from 'big-integer';

export function exponentiation(a: number, n: number): BigNumber {
  let p = bigInt(1);
  console.log( typeof p);

  for (let i = 1; i <= n; i++) {
    p = p.multiply(a);
  }

  return p;
}
