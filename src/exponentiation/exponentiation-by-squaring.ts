import {BigNumber} from 'big-integer';

export function expBySquaring(x: any, n: number): BigNumber {
  if (n === 0) {
    return 1;
  }

  if (n === 1) {
    return x;
  }

  if (n % 2) {
    return x.multiply(expBySquaring(x.multiply(x), (n - 1) / 2));
  } else {
    return expBySquaring(x.multiply(x), n / 2);
  }
}
