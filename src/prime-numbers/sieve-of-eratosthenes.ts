export function sieveOfEratosthenes(n: number) {
  const divs = [];
  const result = []

  for (let i = 2; i <= n; i++) {
    if (!divs[i]) {
      result.push(i);

      for (let j = i * i; j <= n; j += i) {
        divs[j] = true
      }
    }
  }

  console.log(result);

  return result.length;
}
